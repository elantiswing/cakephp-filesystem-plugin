<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UploadFiles\Controller;

use App\Controller\AppController;

/**
 * CakePHP FilesController
 * @author elant
 */
class FilesController extends AppController {
    
    public function initialize() {
        parent::initialize();
        
        $this->loadModel('UploadFiles.Files');
    }
    
    public function uploadImage () {
        $folder = $this->Files->createImage(
                    $this->request->getData('file'), 
                    $this->request->getData('tmp_id'), 
                    $this->request->getData('type')
                );
        
        return $this->jsonResponse([
            'success' => true, 
            'data' => [
                'folder' => $folder
            ]
        ]);
    }
    
    public function uploadFile () {
        $folder = $this->Files->createDownloadFile(
                    $this->request->getData('file'), 
                    $this->request->getData('tmp_id'), 
                    $this->request->getData('type')
                );
        
        return $this->jsonResponse([
            'success' => true, 
            'data' => [
                'folder' => $folder
            ]
        ]);
    }
    
    public function removeFile() {
        if($this->Files->removeFile($this->request->getData('folder'), $this->request->getData('id'))) {
            return $this->jsonResponse(['success' => true, 'message' => __('Arquivo eliminado correctamente.')]);
        }else {
            return $this->jsonResponse(['success' => false, 'message' => __('Non se puido eliminar o arquivo. Proba de novo ou contacta co administrador da web.')]);
        }
    }
}
