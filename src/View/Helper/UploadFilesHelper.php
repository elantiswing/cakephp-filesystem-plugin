<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UploadFiles\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;

/**
 * CakePHP UploadFilesHelper
 * @author elant
 */
class UploadFilesHelper extends Helper {
    
    public $helpers = ['Form', 'Html'];
    
    private $config;
    private $defaults = [];
    private $tmp_id = NULL;
    
    public function initialize(array $config) {
        parent::initialize($config);
        
        $this->config = Configure::read('UploadFiles.config');
        
        $this->tmp_id = \Cake\Utility\Text::uuid();
        
        $this->defaults = [
            'default_image' => 'https://via.placeholder.com/' . $this->config['images']['big']['width'] . 'x' . $this->config['images']['big']['height'] . '.png?text=Imaxe de mostra',
            'images' => [],
            'imageWidth' => $this->config['images']['big']['width'],
            'imageHeight' => $this->config['images']['big']['height'],
            'files' => []
        ];
    }
    
    public function setModelIdField() {
        return $this->Form->hidden('UploadFiles.tmp_id', ['value' => $this->tmp_id]);
    }
    
    /**
     * Create gallery element
     * @param Array $options
     * @return type
     */
    public function gallery($options = []) {
        $options = array_replace_recursive($this->defaults, $options);
        
        return $this->_View->element('UploadFiles.dropzone/gallery', [
                    'tmp_id' => $this->tmp_id,
                    'type' => 'Gallery',
                    'url' => '/UploadFiles/files/uploadImage',
                    'removeUrl' => '/UploadFiles/files/removeFile',
                    'token' => $this->getView()->getRequest()->getParam('_csrfToken'),
                    'files' => $options['files'],
                    'accept' => 'image/*'
                ]);
    }
    
    /**
     * Create gallery element
     * @param Array $options
     * @return type
     */
    public function downloads($options = []) {
        $options = array_replace_recursive($this->defaults, $options);
        
        return $this->_View->element('UploadFiles.dropzone/downloads', [
                    'tmp_id' => $this->tmp_id,
                    'type' => 'Downloads',
                    'url' => '/UploadFiles/files/uploadFile',
                    'removeUrl' => '/UploadFiles/files/removeFile',
                    'token' => $this->getView()->getRequest()->getParam('_csrfToken'),
                    'files' => $options['files'],
                    'accept' => NULL
                ]);
    }
    
    /**
     * Create cropper element
     * @param Array $options
     * @return type
     */
    public function cropper ($options = []) {
        $options = array_replace_recursive($this->defaults, $options);
        
        return $this->_View->element('UploadFiles.cropper', [
                    'tmp_id' => $this->tmp_id,
                    'container' => $this->tmp_id,
                    'image' => $options['default_image'],
                    'imageWidth' => $options['imageWidth'],
                    'imageHeight' => $options['imageHeight'],
                ]);
    }
    
    public function videoIframe($iframe = null) {
        return $this->Form->control('UploadFiles.videoIframe', [
            'label' => false,
            'placeholder' => __('Pega o iframe do video (Redenasa, Youtube ou Vimeo)'),
            'type' => 'textarea',
            'value' => $iframe
        ]);
    }
    
    public function mainFile(array $files, $options = []) {
        $defaults = [
            'type' => 'image',
            'size' => 'medium'
        ];
        
        $options = array_replace_recursive($defaults, $options);
        
        if(!empty($files['Main']) && $options['type'] == 'image') {
            return $this->Html->image($files['Main'][0]->getURL($options['size'] . '.jpeg'), $options);
        }
        
        if(!empty($files['Video']) && $options['type'] == 'video') {
            return '<div class="embed-responsive embed-responsive-16by9">' . $files['Video'][0]->iframe . '</div>';
        }
        
        return '';
    }
}
