<div>
    <?
        echo $this->Html->image($image, ['id' => 'cropper--image']);
    ?>
</div>

<div id="cropper--controls" class="disabled">
    <a href="#" id="cropper--rotate_left" title="Rotate left"><i class="fa fa-rotate-left"></i></a>
    <a href="#" id="cropper--zoom_out" title="Zoom out"><i class="fa fa-search-minus"></i></a>
    <a href="#" id="cropper--fit" title="Fit image"><i class="fa fa-arrows-alt"></i></a>
    <a href="#" id="cropper--zoom_in" title="Zoom in"><i class="fa fa-search-plus"></i></a>
    <a href="#" id="cropper--rotate_right" title="Rotate right"><i class="fa fa-rotate-right"></i></a>
</div>

<? 
    echo $this->Form->hidden('UploadFiles.cropper.x', ['id' => 'cropper--x', 'value' => 0]);
    echo $this->Form->hidden('UploadFiles.cropper.y', ['id' => 'cropper--y', 'value' => 0]);
    echo $this->Form->hidden('UploadFiles.cropper.scale', ['id' => 'cropper--scale', 'value' => 1]);
    echo $this->Form->hidden('UploadFiles.cropper.angle', ['id' => 'cropper--angle', 'value' => 0]);
    echo $this->Form->hidden('UploadFiles.cropper.width', ['id' => 'cropper--w', 'value' => 0]);
    echo $this->Form->hidden('UploadFiles.cropper.height', ['id' => 'cropper--h', 'value' => 0]);
    
    echo $this->UploadFiles->setModelIdField();
?>

<div class="file-group">
    <button class="btn btn-block btn-primary file-browser" type="button"><? echo __('Sube ou cambia a imaxe') ?></button>
    <?
        echo $this->Form->control('UploadFiles.cropper.image', [
                'label' => false,
                'type' => 'file',
                'id' => 'image'
            ]);
    ?>
</div>


<? $this->append('scriptBottom') ?>
<script>
    app.ready(function() {
        var picture = $('#cropper--image');
        
        $('#image').on('change', function() {
            var file = $(this)[0].files[0];
            if (file) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function(e) {
                    // browser completed reading file - display it
                    $('#cropper--image').attr('src', e.target.result);
                    $('#cropper--controls').removeClass('disabled');
                };
            }else {
                $('#cropper--image').attr('src', null);
                $('#cropper--controls').addClass('disabled');
            }
        });

        // Make sure the image is completely loaded before calling the plugin
        picture.on('load', function(){
            var inst = $(this);

            // Remove any existing instance
            if (inst.guillotine('instance')) { 
                inst.guillotine('remove');
                
                $('#cropper--rotate_left, #cropper--rotate_right, #cropper--fit, #cropper--zoom_in, #cropper--zoom_out').off('click');
            }
            
            // Initialize plugin
            picture.guillotine({
                width: <? echo $imageWidth ?>, 
                height: <? echo $imageHeight ?>,
                eventOnChange: 'guillotinechange'
            });
            
            // Set inital data
            picture.guillotine('fit');
            var data = picture.guillotine('getData');
            setImageCropData(data);

            // Bind buttons, only the first time!
            if (! inst.data('bindedBtns')) {
                if(!$('#cropper--controls').hasClass('disabled')) {
                    $('#cropper--rotate_left').click(function(){ picture.guillotine('rotateLeft'); });
                    $('#cropper--rotate_right').click(function(){ picture.guillotine('rotateRight'); });
                    $('#cropper--fit').click(function(){ picture.guillotine('fit'); });
                    $('#cropper--zoom_in').click(function(){ picture.guillotine('zoomIn'); });
                    $('#cropper--zoom_out').click(function(){ picture.guillotine('zoomOut'); });
                }
            }
            
            // Update data on change
            picture.on('guillotinechange', function(ev, data, action) {
                setImageCropData(data);
            });
        });

        // Make sure the 'load' event is triggered at least once (for cached images)
        if (picture.prop('complete')) picture.trigger('load');
        
        function setImageCropData(data) {
            for(var key in data) { 
                $('#cropper--' + key).val(data[key]); 
            }
        }
        
    });
</script>
<? $this->end() ?>

<? 
    echo $this->Html->css([
                'UploadFiles./plugins/guillotine/css/jquery.guillotine',
                'UploadFiles./css/cropper',
            ], [
                'block' => 'cssTop'
            ]
        );
    
    echo $this->Html->script([
                'UploadFiles./plugins/guillotine/js/jquery.guillotine.min'
            ], [
                'block' => 'scriptBottom'
            ]
        );
?>