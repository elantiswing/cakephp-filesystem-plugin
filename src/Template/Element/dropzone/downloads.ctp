<style>
    .dz-size {
        display: none !important;
    }
</style>

<div 
    id="<? echo $tmp_id ?>"
    data-instance-name="<? echo $type ?>"
    data-provide="dropzone" 
    data-url="<? echo $url ?>"
    data-auto-process-queue="true"
    data-add-remove-links="true"
    data-dict-remove-file="Eliminar"
></div>

<?
    echo $this->UploadFiles->setModelIdField();
    echo $this->Form->hidden('type', ['value' => 'Gallery']);
?>

<? $this->append('scriptBottom') ?>
<script>
    app.ready(function() {
        SetUpDropzone('<? echo $token ?>', '<? echo $tmp_id ?>', '<? echo $type ?>', '<? echo $accept ?>', '<? echo $removeUrl ?>');
        
        // Set files inside current dropzone
        var files = <? echo json_encode($files) ?>;
        if(files.length > 0) {
            files.map(file => addDefaultImage(file));
        }

        function addDefaultImage(file) {
            var thumb = file.type_icon;
            var thisDropzone = Dropzone.instances.find(inst => inst.instanceName === '<? echo $type ?>');
            var mockFile = { 
                name: file.file_name, 
//                    size: 12345, 
                mime_type: 'image/jpeg', 
                folder: file.path,
                type: file.type,
                parent: file.parent,
                id: file.id
            };
            thisDropzone.emit("addedfile", mockFile);
            thisDropzone.emit("thumbnail", mockFile, thumb);
            thisDropzone.emit("complete", mockFile);
//                thisDropzone.emit("reset"); // To show default message
        }
    });
</script>
<? $this->end(); ?>

<? 
    echo $this->Html->script([
                'UploadFiles./js/dropzone'
            ], [
                'block' => 'scriptBottom'
            ]
        );
?>