<?php

namespace UploadFiles\Utility;

use Cake\Core\Configure;
use Cake\Filesystem\Folder;

use \UploadFiles\Model\Entity\Image;

use \Imagine\GD\Imagine;
use \Imagine\Image\ImageInterface;
use \Imagine\Image\Box;
use \Imagine\Image\Point;

abstract class ImageCreator_old {
    
    public function create1(array $image, string $tmp_id, string $type) {
        $image = new Image([
            'image' => $image
        ]);
        
        $folder = self::_createFolder($tmp_id . DS . $type, $image['name']);
        
        $imagine = new Imagine();
        $mode    = ImageInterface::THUMBNAIL_INSET;
        
        $uploadedImage = $imagine->open($image['tmp_name']);
        
        foreach(Configure::read('UploadFiles.config')['images'] as $key => $size) {
            if($key == 'thumb') {
                $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            }
            
            $size = new Box($size['width'], $size['height']);
            
            $uploadedImage
                ->thumbnail($size, $mode)
                ->save($folder->path . DS . $key . '.jpeg');
        }
        
        return $folder->path;
    }
    
    public function create(array $image, string $tmp_id, string $type) {
        $image = new Image($image, $tmp_id, $type);
        
        if($image->hasError()) return false;
        
        $imagine = new Imagine();
        $mode    = ImageInterface::THUMBNAIL_INSET;
        $imagineImage   = $imagine->open($image->getTmpName());
        
        if($image->hasCrop()) {
            $imagineImage->rotate($image->angle)
                        ->crop(
                            new Point(
                                $image->real_x, 
                                $image->real_y
                            ), 
                            new Box(
                                $image->real_width, 
                                $image->real_height
                            )
                        );
        }
        
        debug($image);
        exit();
        
        foreach($this->config['images'] as $key => $size) {
            if($key == 'thumb') {
                $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            }
            
            $size = new Box($size['width'], $size['height']);
            
            $tmpFolder = new Folder(WWW_ROOT . $this->config['tmpFolder'] . DS . $this->tmp_id . DS . $this->config['types'][1] . DS . $image->image_name, true);
            
            $image
                ->thumbnail($size, $mode)
                ->save($tmpFolder->path . DS . $key . '.jpeg');
        }
    }
    
    //-------------------------//
    //    PRIVATE FUNCTIONS    //
    //-------------------------//
    
    private function _createFolder(string $folder, string $fullFilename): Folder {
        $filename = substr($fullFilename, 0, strrpos($fullFilename, "."));
        
        return new Folder(WWW_ROOT . Configure::read('UploadFiles.config')['tmpFolder'] . DS . $folder  . DS . $filename, true);
    }
}