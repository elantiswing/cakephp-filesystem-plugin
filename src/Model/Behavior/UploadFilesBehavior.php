<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UploadFiles\Model\Behavior;

use Cake\ORM\Behavior;

use Cake\Core\Configure;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use Cake\ORM\Query;

use Cake\Filesystem\Folder;

use UploadFiles\Model\Entity\File;

/**
 * CakePHP UploadFilesBehavior
 * @author elant
 */
class UploadFilesBehavior extends Behavior {
    
    private $config;
    private $associatedTable;
    
    public function initialize(array $config): void {
        parent::initialize($config);
        
        $this->associatedTable = $this->getTable()->getAlias();
        
        // Load config
        $this->config = Configure::read('UploadFiles.config');
        
        $this->getTable()
                ->hasMany('UploadFiles.Files')
                ->setForeignKey('model_id')
                ->setConditions(['Files.model' => $this->associatedTable]);
    }
    
    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary) {
        $query->contain(['Files'])
              ->formatResults(function($results) {
                  return $results->map(function($value, $key) {
                        if(!empty($value->files)) {
                            $files = new \Cake\Collection\Collection($value->files);
                            $value->files = $files->groupBy('type')->toArray();
                            $value->clean();
                        }
                        
                        return $value;
                  });
              });
    }
    
    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if(!empty($entity->UploadFiles)){
            $this->saveFiles($entity, $this->associatedTable, $entity->id);
        }
    }
    
    public function saveFiles(EntityInterface $entity, string $model, int $modelId) {
        $this->_saveCropperToTmpFolder($entity->UploadFiles['cropper'], $entity->UploadFiles['tmp_id']);
        
        $entitiesFromTmpFiles = $this->_createEntitiesFromTmpFiles($entity->UploadFiles['tmp_id'], $model, $modelId);
        
        $entitiesFromVideoIframe = $this->_createEntitiesFromVideoIframe($entity, $model, $modelId);
        
        $fileEntities = array_merge($entitiesFromTmpFiles, $entitiesFromVideoIframe);
        
        $this->_saveFilesToDB($fileEntities);
    }
    
    
    
    //-------------------------//
    //    PRIVATE FUNCTIONS    //
    //-------------------------//
    
    private function _saveCropperToTmpFolder(array $cropperData, string $tmpId) {
        $cropperData['scale'] = $cropperData['scale'] == 'nan' ? 1 : $cropperData['scale'];
        $cropper = new \UploadFiles\Model\Entity\Cropper(
                        $cropperData['x'],
                        $cropperData['y'],
                        $cropperData['width'],
                        $cropperData['height'],
                        $cropperData['scale'],
                        $cropperData['angle']
                    );
        $image = new \UploadFiles\Model\Entity\Image($cropperData['image'], $cropper);
        
        $file = new File([
            'archive' => $image,
            'destination_path' => $this->config['tmpFolder'] . DS . $tmpId . DS . $this->config['types'][1]
        ]);
        
        return $file->create();
    }
    
    private function _createEntitiesFromTmpFiles(string $tmpId, string $model, int $modelId): array {
        $parentDir = new Folder(WWW_ROOT . $this->config['tmpFolder'] . DS . $tmpId);
        $tmpFolders = !empty($parentDir->read()[0]) ? $parentDir->read()[0] : [];
        
        $entities = [];
        foreach($tmpFolders as $type) {
            $folder = new Folder($parentDir->path . DS . $type);
            $tmpFiles = !empty($folder->read()[0]) ? $folder->read()[0] : [];
        
            foreach($tmpFiles as $file) {
                $file = new File([
                    'path' => $this->config['tmpFolder'] . DS . $tmpId . DS . $type . DS . $file,
                    'destination_path' => $this->config['destinationFolder'] . DS . $model . DS . $modelId . DS . $type . DS . $file,
                    'type' => $type,
                    'model' => $model,
                    'model_id' => $modelId,
                    'file_name' => $file,
                ]);
                
                if($file->move()) {
                    array_push($entities, $file);
                }
            }
        }
        
        $parentDir->delete();
        
        return $entities;
    }
    
    private function _createEntitiesFromVideoIframe(EntityInterface $entity, $model, $modelId) { 
        if(empty($entity->UploadFiles['videoIframe'])){
            if(!empty($entity->files['Video'][0]->id)) {
                $this->getTable()->Files->delete($entity->files['Video'][0]);
            }
            
            return [];
        }
        
        if(!empty($entity->files['Video'][0]->id)) {
            $file = $entity->files['Video'][0];
            $file->set('archive', new \UploadFiles\Model\Entity\VideoIframe($entity->UploadFiles['videoIframe']));
        }else {
            $file = new File([
                'archive' => new \UploadFiles\Model\Entity\VideoIframe($entity->UploadFiles['videoIframe']),
                'model' => $model,
                'model_id' => $modelId,
                'type' => 'Video',
            ]);
        }
        
        return [ 0 => $file ];
    }
    
    private function _saveFilesToDB(array $entities) {
        if(empty($entities)) {
            return false;
        }
        
        $result = $this->getTable()->Files->saveMany($entities);
        
        // For unique file types, remove duplicated (ex: Main files are unique for project)
        if($result) {
            $this->_deleteDuplicatesForUniqueTypes($result);
        }
        
        return $result;
    }
    
    /**
     * Delete entity if the type is unique (ex: Main files)
     * @param array $entities
     */
    private function _deleteDuplicatesForUniqueTypes(array $entities) {
        $collection = new \Cake\Collection\Collection($entities);
            
        $mainFile = $collection->firstMatch(['type' => 'Main']);

        if($mainFile) {
            $duplicatedFiles = $this->getTable()->Files->find()
                            ->where([
                                'Files.model' => $mainFile->model,
                                'Files.model_id' => $mainFile->model_id,
                                'Files.type' => 'Main'
                            ])
                            ->all();
            
            if(!empty($duplicatedFiles) && $duplicatedFiles->count() > 1) {
                $fileToDelete = $duplicatedFiles->sortBy('modified', SORT_ASC)->first();
                
                $this->getTable()->Files->removeFile($fileToDelete->path, $fileToDelete->id);
            }
        }
    }
}
