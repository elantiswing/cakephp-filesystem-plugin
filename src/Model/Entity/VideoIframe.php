<?php

namespace UploadFiles\Model\Entity;

use Cake\Core\Configure;

use Cake\Core\Exception\Exception;

use UploadFiles\Model\Entity\ArchiveInterface;

use Cake\Filesystem\Folder;

class VideoIframe implements ArchiveInterface {
    
    private $url;
    private $iframe;
    
    public function __construct(string $iframe) {
        $this->iframe = $iframe;
        
        $this->_getUrlFromIframe();
    }
    
    public function create(Folder $folder) {
        if(empty($folder)) {
            throw new Exception(__('Folder can\'t be empty'));
        }
    }
    
    public function getPath(): string {
        return $this->url;
    }
    
    private function _getConfig() {
        return Configure::read('UploadFiles.config');
    }
    
    private function _getUrlFromIframe() {
        $matches = [];
        preg_match('/src="(.*?)(?=\")/', $this->iframe, $matches);
        
        !empty($matches[1]) ? $this->url = $matches[1] : false;
    }
}