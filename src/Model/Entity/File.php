<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UploadFiles\Model\Entity;

use Cake\ORM\Entity;

use Cake\Core\Configure;
use Cake\Filesystem\Folder;

use UploadFiles\Model\Entity\ArchiveInterface;

use Cake\Core\Exception\Exception;

/**
 * CakePHP File
 * @author elant
 */
class File extends Entity {
    
    private $config;
    
    protected $_accessible = [
        '*' => true,
    ];
    
    protected $_virtual = [ 
//        'archive',
//        'tmp_id',
        'type',
        'path',
//        'destination_path',
        'archives',
        'type_icon'
    ];
    
    public function __construct(array $properties = array(), array $options = array()) {
        $this->config = Configure::read('UploadFiles.config');
        
        parent::__construct($properties, $options);
        
        if(empty($this->_properties['path']) 
            && (!empty($this->_properties['model'])
                && !empty($this->_properties['model_id']) 
                && !empty($this->_properties['type']) 
                && !empty($this->_properties['file_name'])
                )
            ) {
            $this->set('path', $this->config['destinationFolder'] . DS . $this->_properties['model'] . DS . $this->_properties['model_id'] . DS . $this->_properties['type'] . DS . $this->_properties['file_name']);
        }
    }
    
    protected function _setArchive(ArchiveInterface $archive) {
        $this->set('path', $archive->getPath());
        
        return $archive;
    }
    
    protected function _getPath() {
        return !empty($this->_properties['path']) ? $this->_properties['path'] : null;
    }
    
    protected function _getArchives() {
        $dir = new Folder(WWW_ROOT . $this->_getPath());
        
        if($dir->inPath(WWW_ROOT . $this->config['destinationFolder'])){
            return $dir->read()[1];
        }
        
        return [];
    }
    
    public function getArchive($type = 'thumb') {
        $archives = $this->_getArchives();
        
        if(empty($archives)) {
            return '';
        }
        
        $archive = preg_grep('/^' . $type . '/i', $archives);
        
        return !empty($archive) ? reset($archive) : $archives[count($archives) -1];
    }
    
    protected function _getTypeIcon() {
        $archive = !empty($this->_getArchives()[0]) ? $this->_getArchives()[0] : null;
        
        if(!$archive) {
            return '';
        }
        
        $extension = explode(".", $archive);
        
        $dir = new Folder(\Cake\Core\App::path('Plugin')[0] . 'UploadFiles' . DS . 'webroot' . DS . 'img' . DS . 'file_types' . DS);
        
        if(!$dir->path) {
            return '';
        }
        
        $path = $dir->path . end($extension) . '.svg';
        
        if(file_exists($path)) {
            return \Cake\Routing\Router::fullBaseUrl() . DS . 'UploadFiles' . DS . 'img' . DS . 'file_types' . DS . end($extension) . '.svg';
        }
        
        return \Cake\Routing\Router::fullBaseUrl() . DS . 'UploadFiles' . DS . 'img' . DS . 'file_types' . DS . 'file.svg';
    }

    protected function _getDestinationPath() {
        return $this->_properties['destination_path'];
    }
    
    protected function _getIframe() {
        if(strpos($this->_properties['path'], 'youtube' ) !== false || strpos($this->_properties['path'], 'youtu.be' ) !== false) {
            return $this->_youtubeIframe();
        }else if(strpos($this->_properties['path'], 'vimeo' ) !== false) {
            return $this->_vimeoIframe();
        }else if(strpos($this->_properties['path'], 'redenasa' ) !== false) {
            return $this->_redenasaIframe();
        }
        
        return false;
    }
    
    public function getURL($archiveName = NULL) {
        if(empty($archiveName)) {
            $archiveName = $this->_getArchives()[0];
        }
        
        if(!empty($this->_properties['path'])) {
            return \Cake\Routing\Router::fullBaseUrl() . str_replace('\\', '/', $this->_properties['path'] . DS . $archiveName);
        }
        
        return '';
    }
    
    public function create() {
        $dir = new Folder(WWW_ROOT . $this->_properties['destination_path'], true);
        
        $this->archive->create($dir);
        
        return $this->_properties['destination_path'] . DS . $this->archive->getName();
    }
    
    public function move() {
        $current = new Folder(WWW_ROOT . $this->_properties['path']);
        $destination = new Folder(WWW_ROOT . $this->_properties['destination_path'], true);
        
        if(empty($current->path) || $current->findRecursive() == []){
            return false;
        }
        
        $this->set('path', $this->_properties['destination_path']);
        
        return $current->move($destination->path);
    }
    
    public function delete(): bool {
        $dir = new Folder(WWW_ROOT . $this->_getPath());
        
        $this->_getPathParts($dir);
        
        $result = $dir->delete();
        
        // If parent has no files, delete
        $parentFolder = new Folder(WWW_ROOT . $this->parent_path);
        
        if($parentFolder->findRecursive() == []) {
            $parentFolder->delete();
        }
        
        return $result;
    }
    
    
    //-------------------------//
    //    PRIVATE FUNCTIONS    //
    //-------------------------//
    
    private function _getPathParts(Folder $dir) {
        if($dir->inPath(WWW_ROOT . $this->config['tmpFolder'])){
            
            $folder = $this->config['tmpFolder'];
            $part = str_replace(WWW_ROOT . $folder . DS, '', $dir->path);
            list($parent, $type) = explode(DS, $part);
            
            $this->set('parent_path', $folder . DS . $parent);
            
        }else if($dir->inPath(WWW_ROOT . $this->config['destinationFolder'])) {
            
            $folder = $this->config['destinationFolder'];
            $part = str_replace(WWW_ROOT . $folder . DS, '', $dir->path);
            list($model, $parent, $type) = explode(DS, $part);
            
            $this->set('parent_path', $folder . DS . $model . DS . $parent);
            
        }else {
            throw new Exception(__('Folder doesn\'t exist'));
        }
        
        $this->set('parent', $parent);
        $this->set('type', $type);
    }
    
    private function _redenasaIframe() {
        return '<iframe src="' . $this->_properties['path'] . '" class="embed-responsive-item" frameborder"0" allowfullscreen="allowfullscreen"></iframe>';
    }
    
    private function _vimeoIframe() {
        return '<iframe src="' . $this->_properties['path'] . '" class="embed-responsive-item" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';
    }
    
    private function _youtubeIframe() {
        return '<iframe src="' . $this->_properties['path'] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    }
}
