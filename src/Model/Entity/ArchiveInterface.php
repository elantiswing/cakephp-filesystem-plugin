<?php

namespace UploadFiles\Model\Entity;

use Cake\Filesystem\Folder;

interface ArchiveInterface {
    
//    public function getTmpName(): string;
    
    public function create(Folder $folder);
    
    public function getPath(): string;
}