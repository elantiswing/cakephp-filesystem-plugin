<?php

namespace UploadFiles\Model\Entity;

use Cake\Core\Configure;

use Cake\Core\Exception\Exception;

use UploadFiles\Model\Entity\BaseArchive;
use UploadFiles\Model\Entity\ArchiveInterface;
use UploadFiles\Model\Entity\Cropper;

use Cake\Filesystem\Folder;

use Imagine\Gd\Imagine;
use Imagine\Image\ImageInterface;
use Imagine\Image\Box;
use Imagine\Image\Point;

class Image extends BaseArchive implements ArchiveInterface {
    
    public $cropper;
    
    public function __construct(array $archive, Cropper $cropper = NULL) {
        parent::__construct($archive);
        
        $this->cropper  = $cropper;
    }
    
    public function hasCropper() {
        return !empty($this->cropper);
    }
    
    public function create(Folder $folder) {
        if(empty($folder->path)) {
            throw new Exception(__('Folder can\'t be empty'));
        }
        
        if($this->hasError()) return false;
        
        $imagine = new Imagine();
        $mode    = ImageInterface::THUMBNAIL_INSET;
        $imagineImage   = $imagine->open($this->getTmpName());
        
        $this->_cropImage($imagineImage);
        
        $folder->create($folder->path . DS . $this->getName());
        $folder->cd($this->getName());
        
        foreach($this->_getConfig()['images'] as $key => $size) {
            if($key == 'thumb') {
                $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            }
            
            $size = new Box($size['width'], $size['height']);
            
            $imagineImage
                ->thumbnail($size, $mode)
                ->save($folder->path . DS . $key . '.jpeg');
        }
    }
    
    private function _cropImage(\Imagine\Gd\Image $image) {
        if(!$this->hasCropper()) {
            return false;
        }
        
        $image->rotate($this->cropper->getAngle())
                    ->crop(
                        new Point(
                            $this->cropper->getRealX(), 
                            $this->cropper->getRealY()
                        ), 
                        new Box(
                            $this->cropper->getRealWidth(), 
                            $this->cropper->getRealHeight()
                        )
                    );
    }
    
    private function _getConfig() {
        return Configure::read('UploadFiles.config');
    }
}