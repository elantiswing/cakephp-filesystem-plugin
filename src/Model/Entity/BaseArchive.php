<?php

namespace UploadFiles\Model\Entity;

class BaseArchive {
    
    protected $archive;
    
    public function __construct(array $archive) {
        $this->archive = $archive;
    }
    
    public function getTmpName(): string {
        return $this->archive['tmp_name'];
    }
    
    public function getPath(): string {
        return pathinfo($this->archive['tmp_name'], PATHINFO_DIRNAME);
    }
    
    public function getError() {
        return $this->archive['error'];
    }
    
    public function getFullName() {
        return $this->archive['name'];
    }
    
    public function getName() {
        $name = $this->archive['name'];
        return substr($name, 0, strrpos($name, "."));
    }
    
    public function getType() {
        return $this->archive['type'];
    }
    
    public function getSize() {
        return $this->archive['size'];
    }
    
    public function hasError() {
        return $this->archive['error'] !== 0;
    }
}