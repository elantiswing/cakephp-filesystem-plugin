<?php

namespace UploadFiles\Model\Entity;

class Cropper {
    
    private $x;
    private $y;
    private $width;
    private $height;
    private $scale;
    private $angle;
    
    public function __construct(float $x, float $y, float $width, float $height, float $scale = 1, float $angle = 0) {
        $this->x = $x;
        $this->y = $y;
        $this->width = $width;
        $this->height = $height;
        $this->scale = $scale;
        $this->angle = $angle;
    }
    
    function getX() {
        return $this->x;
    }

    function getY() {
        return $this->y;
    }

    function getWidth() {
        return $this->width;
    }

    function getHeight() {
        return $this->height;
    }

    function getScale() {
        return $this->scale;
    }

    function getAngle() {
        return $this->angle;
    }

    function setX($x) {
        $this->x = $x;
    }

    function setY($y) {
        $this->y = $y;
    }

    function setWidth($width) {
        $this->width = $width;
    }

    function setHeight($height) {
        $this->height = $height;
    }

    function setScale($scale) {
        $this->scale = $scale;
    }

    function setAngle($angle) {
        $this->angle = $angle;
    }
   
    public function getRealX() {
        return $this->x / $this->scale;
    }
    
    public function getRealY() {
        return $this->y / $this->scale;
    }
    
    public function getRealWidth() {
        return $this->width / $this->scale;
    }
    
    public function getRealHeight() {
        return $this->height / $this->scale;
    }
}