<?php

namespace UploadFiles\Model\Entity;

use Cake\Core\Configure;

use Cake\Core\Exception\Exception;

use UploadFiles\Model\Entity\BaseArchive;
use UploadFiles\Model\Entity\ArchiveInterface;

use Cake\Filesystem\Folder;

class DownloadFile extends BaseArchive implements ArchiveInterface {
    
//    public function __construct(array $archive) {
//        parent::__construct($archive);
//        
//    }
    
    public function create(Folder $folder) {
        if(empty($folder->path)) {
            throw new Exception(__('Folder can\'t be empty'));
        }
        
        if($this->hasError()) return false;
        
        $folder->create($folder->path . DS . $this->getName());
        $folder->cd($this->getName());
        
        move_uploaded_file($this->getTmpName() , $folder->path . DS . $this->getFullName());
    }
    
    private function _getConfig() {
        return Configure::read('UploadFiles.config');
    }
}