<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UploadFiles\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;

use Cake\Core\Configure;
use Cake\Filesystem\Folder;

use UploadFiles\Model\Entity\File;

/**
 * CakePHP FilesTable
 * @author elant
 */
class FilesTable extends Table {
    
    private $config;
    
    public function initialize(array $config): void {
        parent::initialize($config);
        
        $this->setTable('files');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        
        $this->addBehavior('Timestamp');
        
        // Load config
        $this->config = Configure::read('UploadFiles.config');
    }
    
    /**
     * Create image from uploaded file. Uploads to tmp folder
     * @param array $image
     * @param string $tmp_id
     * @param string $type
     * @return string - path string
     */
    public function createImage(array $image, string $tmp_id, string $type): string {
        $file = new File([
            'archive' => new \UploadFiles\Model\Entity\Image($image),
            'destination_path' => $this->config['tmpFolder'] . DS . $tmp_id . DS . $type
        ]);
        
        return $file->create();
    }
    
    /**
     * Create a file to downloads from uploaded file. Uploads to tmp folder
     * @param array $downloadFile
     * @param string $tmp_id
     * @param string $type
     * @return string - path string
     */
    public function createDownloadFile(array $downloadFile, string $tmp_id, string $type): string {
        $file = new File([
            'archive' => new \UploadFiles\Model\Entity\DownloadFile($downloadFile),
            'destination_path' => $this->config['tmpFolder'] . DS . $tmp_id . DS . $type
        ]);
        
        return $file->create();
    }
    
    /**
     * Remove file from folder and DB
     * @param string $folder
     * @param int $id
     * @return bool
     */
    public function removeFile(string $folder, int $id = NULL): bool {
        $dir = new Folder(WWW_ROOT . $folder);
        
        $file = new File([
            'path' => $folder
        ]);
        $file->delete();
        
        // If not in tmp directory, delete from DDBB
        if(!$dir->inPath(WWW_ROOT . $this->config['tmpFolder']) && !empty($id)) {
            return $this->_removeFileFromDB($id);
        }
        
        return true;
    }
    
    //-------------------------//
    //    PRIVATE FUNCTIONS    //
    //-------------------------//
    
    private function _removeFileFromDB(string $id) {
        $file = $this->get($id);
        
        if($file) {
            return $this->delete($file);
        }
        
        return false;
    }
}
