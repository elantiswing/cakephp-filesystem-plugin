<?php

use Cake\Routing\Router;

Router::plugin(
    'UploadFiles',
    ['path' => '/UploadFiles'],
    function ($routes) {
        $routes->post('/files/:action', ['controller' => 'Files']);
    }
);