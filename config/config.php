<?php

return [
    'UploadFiles' => [
        'config' => [
            'tmpFolder' => DS . 'files' . DS . 'tmp',
            'destinationFolder' => DS . 'files',
            'types' => [
                1 => 'Main',
                2 => 'Gallery',
                3 => 'Downloads'
            ],
            'images' => [
                'big' => [
                    'width' => 1600,
                    'height' => 900
                ],
                'medium' => [
                    'width' => 800,
                    'height' => 450
                ],
                'small' => [
                    'width' => 300,
                    'height' => 168.75
                ],
                'thumb' => [
                    'width' => 120,
                    'height' => 120
                ],
            ]
        ]
    ]
];