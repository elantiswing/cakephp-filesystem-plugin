function SetUpDropzone(csrfToken, tmpId, type, accept, removeUrl) {
    var inst = Dropzone.instances.find(inst => $(inst.element).data('instance-name') === type);
    
    inst.instanceName = $(inst.element).data('instance-name');

    inst.options.headers = {'X-CSRF-TOKEN': csrfToken};
    inst.options.acceptedFiles = accept;

    // Add tmp folder id and type to params
    inst.on("sending", function(file, xhr, formData) {
        formData.append("tmp_id", tmpId);
        formData.append("type", type);
    });

    // Set uploaded folder to file object
    inst.on("success", function(file) {
        var response = JSON.parse(file.xhr.response);

        file.folder = response.data.folder;
    });

    // Remove uploaded folder
    inst.on("removedfile", function(file) {
        $.ajax({
            url: removeUrl,
            type: 'POST',
            data: {
                folder: file.folder, 
                id: file.id
            },
            headers: {
                'X-CSRF-Token': csrfToken
            },
            success: function(result){
                app.toast(result.message);
            },
            error: function (x, status, error) {
                app.toast('Houbo un erro, si este erro persiste ponte en contacto co administrador da web.');
            }
        });
    });
}