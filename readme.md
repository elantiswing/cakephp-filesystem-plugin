# Cakephp FileSystem

Full filesystem to manage files with Cakephp 3

## Features
- File uploader: The file uploader uses [DropzoneJs](https://www.dropzonejs.com/ "DropzoneJs") to select files and upload to Model's folder
- Images: Uses [GuillotineJs](https://guillotine.js.org/ "GuillotineJs") to crop and upload images to Model's folder
- Links: Save links in the database

All the files are stored on the Model's folder and the path is saved into the DB. The file info is added to the response object on requests

## Installation
Use composer to install the package [thedrumz/fileSystem](https://packagist.org/packages/thedrumz/filesystem "thedrumz/fileSystem")

## Usage
- Add the Behaviour into your Model/Table
- Use the Helper **"UploadFiles"** to add the uploader to your views

Enjoy!